import { Component } from '@angular/core';
import { Device } from '@capacitor/device';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  devicePlatform:any;
  deviceModel:any;

  deviceOs:any;

  deviceOsversion:any;

  deviceConstruct:any;

  constructor() {}

  async getDeviceInfo() {
    const info = await Device.getInfo();
    this.devicePlatform = info.platform;
    this.deviceModel = info.model;
    this.deviceOs = info.operatingSystem;
    this.deviceOsversion = info.osVersion;
    this.deviceConstruct = info.manufacturer;

    
    console.log(info);
  }

  
  };
  

