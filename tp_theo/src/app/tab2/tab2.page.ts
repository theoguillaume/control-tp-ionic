import { Component } from '@angular/core';
import { Geolocation } from '@capacitor/geolocation';
import { timeout } from 'rxjs';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor() {}

  coordonnees : any;
  timestamp : any;


  async getGpsInfo() {
    const info = await Geolocation.getCurrentPosition();
    this.coordonnees = info.coords;
    this.timestamp = info.timestamp;
    const pos = await Geolocation.watchPosition(this.coordonnees.latitude, this.coordonnees.longitude);
    console.log(pos);

  }

  

}
