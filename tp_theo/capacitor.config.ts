import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'tp_theo',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
